﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class AddItem : Form
    {
        public AddItem()
        {
            InitializeComponent();
        }

        public Item GetItem()
        {
            return new Item(NameAdd.Text, Convert.ToInt32(priceAdd.Text), Convert.ToInt32(WeightAdd.Text));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(NameAdd.Text != "" && priceAdd.Text != "" && WeightAdd.Text != "")
            {
                this.Close();
            } 
        }

        private void NameAdd_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letter = e.KeyChar;
            if (!Char.IsLetter(letter) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;  
            }

        }

        private void WeightAdd_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)    //если не цифра, то необрабатываем(игнорируем)
            {
                e.Handled = true;
            }
        }

        private void priceAdd_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)    //если не цифра, то необрабатываем(игнорируем)
            {
                e.Handled = true;
            }
        }
    }
}
