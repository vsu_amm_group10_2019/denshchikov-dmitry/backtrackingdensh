﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Backpack
    {
        private int bestPrice;
        private int maxW;
        private List<Item> bestItems;

        public Backpack(int maxW = 50)
        {
            this.maxW = maxW;
        }

        private int madeMaxWidht(List<Item> items)
        {
            int max = 0;
            foreach(Item i in items)
            {
                max += i.Weight;
            }
            return max;
        }

        private int madeMaxPrice(List<Item> items)
        {
            int max = 0;
            foreach (Item i in items)
            {
                max += i.Price;
            }
            return max;
        }

        private void CheckSet(List<Item> items)
        {
            if(bestItems == null && madeMaxWidht(items) <= maxW)
            {
                bestItems = items;
                bestPrice = madeMaxPrice(items);
            } else
            {
                if(madeMaxPrice(items) > bestPrice && madeMaxWidht(items) <= maxW)
                {
                    bestItems = items;
                    bestPrice = madeMaxPrice(items);
                }
            }              
        } 

        public void madeBestSet(List<Item> items)
        {
            if (items.Count > 0)
                CheckSet(items);

            for(int i = 0; i < items.Count; i++)
            {
                List<Item> itemsSet = new List<Item>(items);
                itemsSet.RemoveAt(i);
                madeBestSet(itemsSet);
            }
        }

        //возвращает решение задачи (набор предметов)
        public List<Item> GetBestSet()
        {
            return bestItems;
        }
    }
}
