﻿
namespace WindowsFormsApp1
{
    partial class AddItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.NameAdd = new System.Windows.Forms.TextBox();
            this.priceAdd = new System.Windows.Forms.TextBox();
            this.WeightAdd = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(79, 327);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 38);
            this.button1.TabIndex = 0;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(112, 251);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Цена";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Вес";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(94, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Название";
            // 
            // NameAdd
            // 
            this.NameAdd.Location = new System.Drawing.Point(86, 93);
            this.NameAdd.Name = "NameAdd";
            this.NameAdd.Size = new System.Drawing.Size(100, 26);
            this.NameAdd.TabIndex = 4;
            this.NameAdd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NameAdd_KeyPress);
            // 
            // priceAdd
            // 
            this.priceAdd.Location = new System.Drawing.Point(86, 274);
            this.priceAdd.Name = "priceAdd";
            this.priceAdd.Size = new System.Drawing.Size(100, 26);
            this.priceAdd.TabIndex = 5;
            this.priceAdd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.priceAdd_KeyPress);
            // 
            // WeightAdd
            // 
            this.WeightAdd.Location = new System.Drawing.Point(86, 194);
            this.WeightAdd.Name = "WeightAdd";
            this.WeightAdd.Size = new System.Drawing.Size(100, 26);
            this.WeightAdd.TabIndex = 6;
            this.WeightAdd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WeightAdd_KeyPress);
            // 
            // AddItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 389);
            this.Controls.Add(this.WeightAdd);
            this.Controls.Add(this.priceAdd);
            this.Controls.Add(this.NameAdd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddItem";
            this.Text = "Добавление";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NameAdd;
        private System.Windows.Forms.TextBox priceAdd;
        private System.Windows.Forms.TextBox WeightAdd;
    }
}