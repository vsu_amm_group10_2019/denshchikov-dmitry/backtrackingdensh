﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MainForm : Form
    {
        List<Item> items = new List<Item>();
        public MainForm()
        {
            InitializeComponent();           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddItem addItem = new AddItem();
            addItem.ShowDialog();
            Item item = addItem.GetItem();
            items.Add(item);
            itemsListView.Items.Add(new ListViewItem(new string[] { item.Name, item.Weight.ToString(), item.Price.ToString() }));       
        }

        private void ShowList(List<Item> items)
        {
            itemsListView.Items.Clear();

            foreach (Item i in items)
            {
                itemsListView.Items.Add(new ListViewItem(new string[] { i.Name, i.Weight.ToString(), i.Price.ToString()}));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Backpack backpack = addMaxW.Text == "" ? new Backpack() : new Backpack(Convert.ToInt32(addMaxW.Text));
            backpack.madeBestSet(items);
            List<Item> solve = backpack.GetBestSet();
            if(solve == null)
            {
                MessageBox.Show("Нет решения");
            } else
            {
                itemsListView.Items.Clear();
                ShowList(solve);
                MessageBox.Show("Решение приведено в таблице");
            }
        }
    }
}
